#include "jni.h"
#include "converter_MilesToLiters.h"
#include "gtest/gtest.h"

extern "C" {
double cmtl(double);
}

TEST(MilesToLitersTest, Equal) {
  EXPECT_NEAR(5.6003472163, Java_converter_MilesToLiters_convertm2l(NULL, NULL, 42), 0.0000001);
}

TEST(MilesToLitersASMTest, Equal) {
  EXPECT_NEAR(5.6003472163, cmtl(42), 0.0000001);
}

