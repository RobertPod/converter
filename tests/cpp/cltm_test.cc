#include "jni.h"
#include "converter_LitersToMiles.h"
#include "gtest/gtest.h"

extern "C" {
double cltm(double);
}

TEST(LitersToMilesTest, Equal) {
  EXPECT_NEAR(2.50228279877, Java_converter_LitersToMiles_convertl2m(NULL, NULL, 94), 0.0000001);
}

TEST(LitersToMilesASMTest, Equal) {
  EXPECT_NEAR(2.50228279877, cltm(94), 0.0000001);
}
