package converter;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class MilesToLitersTest {

  @Test
  @DisplayName(" - The Answer to the Great Question... Of Life, the Universe and Everything... Is... Forty-two")
  void convertFortyTwoMilesToLiters() {
    MilesToLiters mtl = new MilesToLiters(); 
    assertEquals(5.6003472163, mtl.convert(42), 0.0000001, "Said Deep Thought, with infinite majesty and calm. I have expected: 5.6003472163");
  }

}
