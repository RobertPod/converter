# Java based units converter

This Java based code converts miles per gallon to liters per 100km and vice versa. It is used by another project: <a href="https://gitlab.com/mkowsiak/converter-docker">converter-docker</a>.

This is heavilly over-enginered code that uses various patterns, different languages and binds together `Java` and native code.

I know that converting liters to miles and vice versa is quite simple. All you have to do is calculate

```
ltm(x) = 100 * 0.621371192 / (x * 0.264172052)
mtl(x) = 3.78541178 * 100 / (x * 1.609344)
```

But hey, why not to make it little bit more complicated.

![](./images/uml.png)

# Project structure

Source code is divided inside directories based on source language. Tests are grouped inside `tests` and divided base on testing framework.

```
.
|-- Makefile                                - Makefile for building shared libraries and compiling code
|-- Makefile.common
|-- README.md                               - this README.md
|-- asm
|   |-- cltm.s.darwin                       - assembler code for macOS
|   |-- cltm.s.linux                        - assembler code for Linux
|   |-- cmtl.s
|   |-- cmtl.s.darwin                       - assembler code for macOS
|   `-- cmtl.s.linux                        - assembler code for Linux
|-- c
|   |-- converter_LitersToMiles.cc          - JNI based C code that calls assembler
|   `-- converter_MilesToLiters.cc          - JNI based C code that calls assembler
|-- images
|   `-- uml.png
|-- jar
|-- java
|   `-- converter
|       |-- Converter.java                  - Interface of all the converters
|       |-- CustomConverterFactory.java     - Factory class for custom converters
|       |-- LitersToMiles.java              - Custom converter from miles to liters
|       |-- Main.java                       - Main class
|       `-- MilesToLiters.java              - Custom converter from liters to miles
|-- lib
|-- obj
|-- target
`-- tests
    |-- cpp
    |   |-- cltm_test.cc                    - google test based tests of liters to miles converter
    |   `-- cmtl_test.cc                    - google test based tests of miles to liters converter
    `-- java
        `-- converter
            |-- LitersToMilesTest.java      - JUnit 5 based tests of Java code
            `-- MilesToLitersTest.java
```

# Building

    > git clone https://gitlab.com/mkowsiak/converter.git
    > make clean
    > make

# Running

    > make VALUE=${VALUE} FROM=${FROM} calculate

# Examples

    # converting 9.9 liters per 100km to miles per gallon
    > make VALUE=9.9 FROM=liters calculate

    # converting 25 miles per gallon to liters per 100km
    > make VALUE=25 FROM=miles calculate

# Running tests

You can run tests, but you will need `googletest` installed in order to compile `C++` based test cases. Running tests requires `GOOGLE_TEST` to be exported in your environment. This variable have to point at place where you can find `${GOOGLE_TEST}/include/gtest/gtest.h` and `${GOOGLE_TEST}/lib/libgtest.a`.

Once you have it in place, you can run

```
> make junit-test-run
> make google-test-run
```

These tests will run `JUnit` based tests for components developed using `Java`. There is a `googletest` based set of tests for `C++` and `assembler` based parts of the code.

# Known limitations

At the moment error handling is not quite efficient. This project is used only inside <a href="https://gitlab.com/mkowsiak/converter-docker">converter-docker</a>.

# Java code used for converting

`converter-docker` is based on simple `Java` code that is based on singleton patter with factory pattern applied to native code wrappers. Calculations are performed by `C` based code executed via `JNI`. This code calls routines developed in `assembler`.

You can find source code of `converter-docker` here: <a href="https://gitlab.com/mkowsiak/converter-docker">converter-docker</a>.

