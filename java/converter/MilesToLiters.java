package converter;

public class MilesToLiters implements Converter {

  static {
    System.loadLibrary("MilesToLiters");
  }

  public native double convertm2l(double val);

  public double convert(double val) {
    return convertm2l(val);
  }

}
