include Makefile.common

assert = $(if $2,$(if $1,,$(error $2)))

JUnit=1.5.2
JUnit.JAR=junit-platform-console-standalone-$(JUnit).jar

all: dirs compilejava compilec
	$(call assert,$(GOOGLE_TEST),GOOGLE_TEST is not defined)

dirs:
	-mkdir -p jar
	-mkdir -p obj
	-mkdir -p target
	-mkdir -p lib

compilejava:
	@$(JAVA_HOME)/bin/javac -h c -d target java/converter/*.java

compilec: compilejava
	as -c -o obj/cltm.o asm/cltm.s.$(ARCH)
	as -c -o obj/cmtl.o asm/cmtl.s.$(ARCH)
	g++ -g -shared -fpic -I${JAVA_HOME}/include -I${JAVA_HOME}/include/$(ARCH) c/converter_LitersToMiles.cc obj/cltm.o -o lib/libLitersToMiles.$(EXT)
	g++ -g -shared -fpic -I${JAVA_HOME}/include -I${JAVA_HOME}/include/$(ARCH) c/converter_MilesToLiters.cc obj/cmtl.o -o lib/libMilesToLiters.$(EXT)

junit-test-compile: junit-download
	@$(JAVA_HOME)/bin/javac -cp target:jar/$(JUnit.JAR) -d target tests/java/converter/*.java

junit-test-run: compilec junit-test-compile
	@$(JAVA_HOME)/bin/java -Djava.library.path=$(LD_LIBRARY_PATH):./lib -jar jar/$(JUnit.JAR) --class-path target --scan-class-path

junit-download:
	@curl -s -z jar/$(JUnit.JAR) \
          -o jar/$(JUnit.JAR) \
          http://central.maven.org/maven2/org/junit/platform/junit-platform-console-standalone/$(JUnit)/$(JUnit.JAR)

google-test-compile: compilec
	g++ -std=c++11 -o obj/cltm_test \
	tests/cpp/cltm_test.cc c/converter_LitersToMiles.cc obj/cltm.o \
	-I./c -I${JAVA_HOME}/include -I${JAVA_HOME}/include/$(ARCH) -I${GOOGLE_TEST}/include \
	$(PTHREAD) \
	${GOOGLE_TEST}/lib/libgtest.a \
	${GOOGLE_TEST}/lib/libgtest_main.a
	
	@g++ -std=c++11 -o obj/cmtl_test \
	tests/cpp/cmtl_test.cc c/converter_MilesToLiters.cc obj/cmtl.o \
	-I./c -I${JAVA_HOME}/include -I${JAVA_HOME}/include/$(ARCH) -I${GOOGLE_TEST}/include \
	$(PTHREAD) \
	${GOOGLE_TEST}/lib/libgtest.a \
	${GOOGLE_TEST}/lib/libgtest_main.a

google-test-run: google-test-compile
	@./obj/cltm_test
	@./obj/cmtl_test

calculate:
	@$(JAVA_HOME)/bin/java -Djava.library.path=$(LD_LIBRARY_PATH):./lib -cp target converter.Main $(FROM) $(VALUE)

clean:
	@-rm -rfv target/*
	@-rm c/*.h
	@-rm obj/*
	@-rm -rfv lib/*
	@-rm -rf jar/*
