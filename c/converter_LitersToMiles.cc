#include <stdio.h>
#include "jni.h"
#include "converter_LitersToMiles.h"

#define ASM

#ifdef ASM
extern "C" {
extern double cltm(double);
}
#else
#define cltm(x) 100 * 0.621371192 / (x * 0.264172052)
#endif

JNIEXPORT jdouble JNICALL Java_converter_LitersToMiles_convertl2m
  (JNIEnv *env, jobject obj, jdouble value) {

 return cltm( value );

}
